public class Skips {

        private int name;
        private String threat;
        private String tags[];
        private boolean warning;
        private char clearance;

        public int getName() {
            return name;
        }

        public void setName(int name) {
            this.name = name;
        }
        public String getThreat() {
            return threat;
        }

        public void setThreat(String threat) {
            this.threat = threat;
        }
        public String[] getTags() {
            return tags;
        }

        public void setTags(String[] tags) {
            this.tags = tags;
        }
        public boolean getWarning() {
            return warning;
        }

        public void setWarning(boolean warning) {
            this.warning = warning;
        }
        public char getClearance() {
            return clearance;
        }

        public void setClearance(char clearance) {
            this.clearance = clearance;
        }

        public static String[] p005 = {"An ornate key, the design of which is similar to those produced in the 1920s.",
        "SCP-005 has the unique ability to open any and all forms of lock, mechanical or digital, with relative ease.",
        "SCP-005 was discovered when a civilian used it to infiltrate a high security facility, stating he 'found' it'.",
        "May be used if under supervision by at least 1 Level 4 as a replacement for lost security passes.",
        "May not be used for vending machine repairs, opening lockers, or for any personnel's spare home key."};

        public Skips(int name, String threat, String[] tags, boolean warning, char clearance) {

            this.name = name;
            this.threat = threat;
            this.tags = tags;
            this.warning = warning;
            this.clearance = clearance;
        }
    }
