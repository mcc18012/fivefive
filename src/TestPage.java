import org.junit.Assert;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import java.util.*;

public class TestPage {

    String[] tags682 = new String[] {"Adaptive","Keter","Reptilian","Self-Repairing","Sentient"};
    String[] tags005 = new String[] {"Adaptive","Key","Metallic","Safe"};
    Skips skip1Test = new Skips(682,"Keter", tags682,false,'A');
    Skips skip2Test = new Skips(005,"Safe", tags005,false,'A');
    Skips skip3Test = null;

    @Test
    @DisplayName("Grouped assertion, all 'name' variables in objects are equal to what they're supposed to be.")
    public void testName() {
        assertAll(
                "ERROR: Invalid SCP Classification",
                () -> assertEquals(skip1Test.getName(),682,"SCP should be # 682"),
                () -> assertEquals(skip2Test.getName(),005,"SCP should be # 005")
        );
    }

    @Test
    @DisplayName("Test to assert object is not null.")
    public void testIfNotNull() {
        assertNotNull(skip1Test, "Entry is null. Contact help desk immediately to rectify.");
    }

    @Test
    @DisplayName("Test to assert object IS null.")
    public void testIfNull() {
        assertNull(skip3Test, "NOTICE: Entry is no longer null. Inform security immediately of code SUPERBLANK.");
    }

    @Test
    @DisplayName("Test to assert that array is equal to array designated.")
    public void testGetTags() {
        assertArrayEquals(skip1Test.getTags(), tags682);
    }

    @Test
    @DisplayName("Test to assert variable 'threat' in object is equal to 'Keter'.")
    public void testSameThreat() {
        assertSame(skip1Test.getThreat(), "Keter");
    }

    @Test
    @DisplayName("Test to assert that object's variable was not mixed up with another.")
    public void testNotSameThreat() {
        assertNotSame(skip1Test.getThreat(), skip2Test.getThreat());
    }

    @Disabled("Disabled until Doctor Miller receives 05 approval for testing")
    @Test
    @DisplayName("Test for all instances of [REDACTED] and/or [REDACTED]")
    public void testREDACTED() {
    }

    @Test
    @DisplayName("CONTAINMENT BREACH: SCP-682")
    public void test682Breach() {
        assertFalse(skip1Test.getWarning());

        if (skip1Test.getWarning()) {
            throw new IllegalArgumentException("CONTAINMENT BREACH CODE BLACK");
        }
    }

    public void getSkip() {
        Skips[] list = new Skips[3];
        list[0] = new Skips(682,"Keter", tags682,false,'A');
        list[1] = new Skips(005,"Safe", tags005,false,'B');
        list[2] = null;

        for (int i = 0; i < 2; i++) {
            String warn;
            try {
                System.out.println("Accessing file.\n");
                System.out.println("Item #: " + list[i].getName() + "\n");
                System.out.println("Object Class: " + list[i].getThreat() + "\n");
                System.out.println("Tags: " + Arrays.toString(list[i].getTags()) + "\n");
                if (list[i].getWarning()) {
                    warn = "Not in containment";
                } else {
                    warn = "Currently contained";
                }
                System.out.println("Status: " + warn + "\n");
                System.out.println("Clearance level : " + list[i].getClearance());
            }
            catch(NullPointerException e)
            {
                System.out.println("NullPointerException occurred. Contact help desk.");
            }
            catch(ArrayIndexOutOfBoundsException e)
            {
                System.out.println("Denied authorization to view further files.");
            }
        }
    }
}
